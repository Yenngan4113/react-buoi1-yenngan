import logo from "./logo.svg";
import "./App.css";
import Header from "./Component/Header";
import Banner from "./Component/Banner";
import Item from "./Component/Item";
import Footer from "./Component/Footer";

function App() {
  return (
    <>
      <Header />
      <div className="container">
        <Banner />
        <div className="row" style={{ margin: "0px" }}>
          <Item />
          <Item />
          <Item />
          <Item />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default App;
